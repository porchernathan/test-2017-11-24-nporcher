#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/* our rules :
 * 4734
 * 7177 < 1 misplaced
 * 4000 < 1 properly placed
 */

//an invalid value for one of the secret digits
#define CONSUMED 10

enum game_result {
	WIN,
	LOSE
};

struct game {
	uint8_t secret[4];
	uint8_t remaining_tries;
	unsigned won_games;
};

struct turn_result {
	uint8_t correctly_placed, misplaced;
};

/* flushes stdin
 * fseek(stdin, 0, SEEK_END) doesn't work at home
 * so I use a portable implementation
 */
void flush_input()
{
	//don't flush the 1st time (the buffer is empty)
	static int first_call = 1;
	if (first_call == 1) {
		first_call = 0;
		return;
	}

	while (getchar() != '\n')
		if (feof(stdin))
			return;
}

/* returns 1 if the player wants to replay
 * and 0 otherwise
 */
int wants_to_replay()
{
	int matches;
	char in;
	for(;;) {
		printf("Do you want to replay ? (y/n)\n");

		flush_input();
		matches = scanf(" %c", &in);

		if (matches == 1 && (in == 'y' || in == 'n'))
			break;

		printf("This input is not valid.\n");
	}

	return in == 'y';
}

/* prompts the user for an uint8_t in [0;9]
 */
uint8_t input_uint8_t()
{
	int matches;
	unsigned in;
	for(;;) {
		printf("Pick a number between 0 and 9.\n");

		flush_input();
		matches = scanf(" %u", &in);

		//in is unsigned so we don't have to check (0 <= in)
		if (matches == 1 && in <= 9)
			return (uint8_t)in;

		printf("This input is not valid.\n");
	}
}

/* prompts the player for a 4-digit attmpt
 */
void input_attempt(uint8_t attempt[4])
{
	printf("Input your attempt :\n");
	for (int i = 0; i < 4; i++)
		attempt[i] = input_uint8_t();
}

/* returns a random value in [min;max]
 */
unsigned random(unsigned min, unsigned max)
{
	assert(min <= max && (max - min + 1) < RAND_MAX);
	return rand() % (max - min + 1) + min;
}

/* sets the secret number
 */
void gen_secret(uint8_t secret[4])
{
	for (int i = 0; i < 4; i++)
		secret[i] = random(0,9);
}

/* looks for an occurence of `n` in `secret`
 * and sets it to `CONSUMED`
 * returns 1 if it found one and 0 otherwise
 */
int found_and_consumed_in(uint8_t n, uint8_t secret[4])
{
	for (int i = 0; i < 4; i++) {
		if (secret[i] == n) {
			secret[i] = CONSUMED;
			return 1;
		}
	}

	return 0;
}

/* returns `turn_result.correctly_placed == 4`
 * if the player found the number
 */
struct turn_result evaluate_attempt(
	const uint8_t secret_immutable[4],
	const uint8_t attempt_immutable[4])
{
/* we try to find the digits of `attempt` in `secret_immutable`
 * we set the digit to `CONSUMED` in `secret` to avoid duplicates
 */
	struct turn_result turn_result = {
		.misplaced = 0,
		.correctly_placed = 0
	};
	uint8_t secret[4], attempt[4];

	//copy secret and attempt
	memcpy(secret, secret_immutable, 4 * sizeof(*secret));
	memcpy(attempt, attempt_immutable, 4 * sizeof(*attempt));

	for (int i = 0; i < 4; i++) {
		if (secret[i] == attempt[i]) {
			turn_result.correctly_placed++;
			secret[i] = CONSUMED;
			attempt[i] = CONSUMED;
		}
	}

	/* only misplaced matches can be found here
	 * because all the correctly placed ones were
	 * consumed in the previous loop
	 */
	for (int i = 0; i < 4; i++)
		if (attempt[i] != CONSUMED
		    && found_and_consumed_in(attempt[i], secret))
			turn_result.misplaced++;

	return turn_result;
}

/* utility for printing "x time(s)"
 * const char *s = plural_str(n);
 * printf("%u time%s", n, s);
 */
const char *plural_str(unsigned n)
{
	return n > 1 ? "s" : "";
}

/* `plural_str` for "are and "is"
 */
const char *plural_are_is(unsigned n)
{
	return n > 1 || n == 0 ? "are" : "is";
}

/* prints num in the form "1234"
 */
void print_num(const uint8_t num[4])
{
	for (int i = 0; i < 4; i++)
		printf("%hhu", num[i]);
}

void print_remainin_tries(unsigned tries)
{
	printf("%hhu tr%s remains.\n", tries, tries > 1 ? "ies" : "y");
}

enum game_result play_game(struct game *game)
{
	struct turn_result turn_result;
	uint8_t attempt[4];
	const char *are_is;

	gen_secret(game->secret);
	printf("The computer picked the number.\n");
	game->remaining_tries = 10;

	for(;;) {
		input_attempt(attempt);
		game->remaining_tries--;

		turn_result = evaluate_attempt(game->secret, attempt);

		if (turn_result.correctly_placed == 4)
			return WIN;

		printf("You tried ");
		print_num(attempt);

		are_is = plural_are_is(turn_result.correctly_placed);
		printf(".\n%hhu %s correctly placed.\n",
			turn_result.correctly_placed,
			are_is);

		are_is = plural_are_is(turn_result.misplaced);
		printf("%hhu %s misplaced.\n", turn_result.misplaced, are_is);

		print_remainin_tries(game->remaining_tries);

		if (game->remaining_tries == 0)
			return LOSE;
	}
}

int main()
{
	struct game game = { .won_games = 0};
	const char *s;

	//seed random
	srand(time(NULL));

	printf("Welcome to the mastermind game !\n"
	       "You have to find the 4-digit number chosen by the computer\n"
	       "Each turn, you will be indicated how many you got right\n"
	       "and how many you found but did't place right.\n\n");
	do {
		if (play_game(&game) == WIN) {
			printf("You won !\n");
			game.won_games++;
		} else {
			printf("You lose.\n");
			printf("The secret number was ");
			print_num(game.secret);
			printf(".\n");
		}

		s = plural_str(game.won_games);
		printf("You won %u time%s.\n", game.won_games, s);
	} while(wants_to_replay());

	return 0;
}
